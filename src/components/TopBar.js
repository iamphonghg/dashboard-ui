import React from 'react';
import { Flex, Icon, Link, List, Button, Text, Image } from '@chakra-ui/react';
import {
  HiChartSquareBar,
  HiLogout,
  HiViewGridAdd,
  HiQuestionMarkCircle,
} from 'react-icons/hi';
import { Link as NavLink, useLocation } from 'react-router-dom';

export default function TopBar() {
  return (
    <Flex className="top-bar-container">
      <Flex className="top-bar-text">
        <Text fontSize="14px">Hello,</Text>
        <Text fontSize="14px" fontWeight="600">
          Nghiave
        </Text>
      </Flex>
      <Image src="" alt="sample avatar"></Image>
    </Flex>
  );
}
