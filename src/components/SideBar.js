/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Flex, Icon, Link, List, Button, Text, Image } from '@chakra-ui/react';
import {
  HiChartSquareBar,
  HiLogout,
  HiViewGridAdd,
  HiQuestionMarkCircle,
} from 'react-icons/hi';
import { Link as NavLink, useLocation } from 'react-router-dom';

const siderBarItems = [
  {
    title: 'Dashboard',
    url: '/dashboard',
    icon: HiChartSquareBar,
  },
  {
    title: 'Apps',
    url: '/dashboard/apps',
    icon: HiViewGridAdd,
  },
  {
    title: 'FAQs',
    url: '',
    icon: HiQuestionMarkCircle,
  },
  // { title: 'Referrals', url: '/dashboard/referrals', icon: HiLightningBolt, },
  // { title: 'Payouts', url: '/dashboard/payouts', icon: HiClipboardList, },
];

export default function SideBar() {
  const location = useLocation();

  return (
    <Flex
      flexDir="column"
      background="white"
      w="220px"
      p="1rem"
      pt="2rem"
      justifyContent="space-between"
      h="100vh"
      className="side-bar-container"
      borderRight="1px solid #e9e9ef"
    >
      <Flex flexShrink="0" flexDirection="column">
        <Flex>
          <Image
            src="../../img/smartify-apps-logo.png"
            alt="Smartify Apps logo"
          />
        </Flex>
        <List mt="2rem">
          {siderBarItems.map(item => (
            <Link
              as={NavLink}
              to={item.url}
              _hover={{
                textDecoration: 'none',
                background: 'primaryColor',
                color: '#fff',
              }}
              display="block"
              mt="0.5rem"
              mb="0.5rem"
              padding="0.6rem 0.8rem"
              borderRadius="0.25rem"
              key={item.title}
              styles={{ bg: 'red' }}
              {...(location.pathname === item.url
                ? { bg: 'primaryColor', color: '#fff' }
                : {})}
              _focus={{}}
            >
              <Flex alignItems="center" gap="0.8rem">
                <Icon as={item.icon} w="6" h="6" />
                <Text
                  // fontSize="lg"
                  lineHeight="none"
                  mt="3px"
                  fontWeight="500"
                  fontSize="14px"
                  color={location.pathname === item.url ? '#fff' : 'textColor'}
                >
                  {item.title}
                </Text>
              </Flex>
            </Link>
          ))}
        </List>
      </Flex>
      <Flex flexDirection="column">
        <Button variant="ghost" rightIcon={<HiLogout />} _hover={{}}>
          Log out
        </Button>
      </Flex>
    </Flex>
  );
}
