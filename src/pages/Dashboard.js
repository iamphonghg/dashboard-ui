import React from 'react';
import { Flex, Box, Text, Icon } from '@chakra-ui/react';
import { Stat, StatLabel, StatNumber, StatHelpText } from '@chakra-ui/react';
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from '@chakra-ui/react';
import {
  referrals,
  pendingPayouts,
  paidPayouts,
} from '../fake-data/dashboard-data';
import { CgCreditCard } from 'react-icons/cg';

const secondaryStatItems = [
  {
    label: 'Total Pending',
    // helptext: 'Unpaid amount of money',
    number: '$123,69',
  },
  {
    label: 'Active Referrals',
    // helptext: 'Number of active installs via your referral links',
    number: '2.626',
  },
  {
    label: 'Active Charges',
    // helptext: 'The amount of money paid to you already',
    number: '262',
  },
];

export default function Dashboard() {
  return (
    <Flex
      flexDirection="column"
      background="#fff"
      w="full"
      alignItems="center"
      gap="80px"
      p="40px 0"
    >
      {/* Summary stat cards section  */}
      <Text
        color="textColor"
        fontWeight="600"
        fontSize="20px"
        w="dashboardContentWidth"
        mb="-60px"
      >
        Welcome back 👋,{' '}
      </Text>
      <Box
        p="30px 40px"
        background="primaryColor"
        w="dashboardContentWidth"
        borderRadius="8px"
      >
        <Flex>
          <Flex gap="30px" mr="120px">
            <Icon as={CgCreditCard} w={20} h={20} color="#fff" />
            <Flex flexDirection="column" justifyContent="center">
              <Text fontSize="16px" fontWeight="500" color="white">
                Total Balance
              </Text>
              <Text fontSize="32px" fontWeight="800" color="white">
                $673.412,66
              </Text>
            </Flex>
          </Flex>
          <Flex w="full" justifyContent="space-between">
            {secondaryStatItems.map(secondaryStatItem => (
              <Flex flexDirection="column" justifyContent="center">
                <Text fontSize="12px" fontWeight="500" color="white">
                  {secondaryStatItem.label}
                </Text>
                <Text fontSize="24px" fontWeight="600" color="white">
                  {secondaryStatItem.number}
                </Text>
              </Flex>
            ))}
          </Flex>
        </Flex>
      </Box>

      {/* Referral section  */}
      <Text
        color="textColor"
        fontWeight="600"
        fontSize="20px"
        mb="-60px"
        w="dashboardContentWidth"
      >
        Total Referrals
      </Text>
      <Box
        p="20px 20px"
        w="dashboardContentWidth"
        border="solid 1px"
        borderColor="cardBorder"
        borderRadius="8px"
      >
        <TableContainer whiteSpace="unset">
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>Shop</Th>
                <Th>Shop Plan</Th>
                <Th>Installed App</Th>
                <Th>Program</Th>
                <Th isNumeric>Commission (%)</Th>
                <Th>Since</Th>
                <Th>Status</Th>
              </Tr>
            </Thead>
            <Tbody>
              {referrals.map(referral => (
                <Tr>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {referral.shop}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {referral.shopPlan}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {referral.installedApp}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {referral.program}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {referral.comission}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {referral.since}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {referral.status}
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </TableContainer>
      </Box>

      <Text
        color="textColor"
        fontWeight="600"
        fontSize="20px"
        mb="-60px"
        w="dashboardContentWidth"
      >
        Balance Updates
      </Text>
      {/* Pending payouts section  */}
      <Box
        p="20px 20px"
        w="dashboardContentWidth"
        border="solid 1px"
        borderColor="cardBorder"
        borderRadius="8px"
      >
        <TableContainer whiteSpace="unset">
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>Customer paid at</Th>
                <Th>Shop</Th>
                <Th>Shop Plan</Th>
                <Th>Installed App</Th>
                <Th isNumeric>Commission (%)</Th>
                <Th isNumeric>Commission value</Th>
                <Th>Status</Th>
              </Tr>
            </Thead>
            <Tbody>
              {pendingPayouts.map(pendingPayout => (
                <Tr>
                  <Td color="secondaryTextColor" fontSize="12px">
                    {pendingPayout.paidAt}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.shop}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.shopPlan}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.installedApp}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.comission}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.comissionValue}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.status}
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </TableContainer>
      </Box>

      {/* Paid payouts section  */}
      <Box
        p="20px 20px"
        w="dashboardContentWidth"
        border="solid 1px"
        borderColor="cardBorder"
        borderRadius="8px"
        display="none"
      >
        <TableContainer whiteSpace="unset">
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>Smartify Apps paid at</Th>
                <Th>Shop</Th>
                <Th>Shop Plan</Th>
                <Th>Installed App</Th>
                <Th isNumeric>Commission (%)</Th>
                <Th isNumeric>Commission value</Th>
                <Th>Status</Th>
              </Tr>
            </Thead>
            <Tbody>
              {pendingPayouts.map(pendingPayout => (
                <Tr>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.paidAt}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.shop}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.shopPlan}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.installedApp}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.comission}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.comissionValue}
                  </Td>
                  <Td fontSize="12px" color="secondaryTextColor">
                    {pendingPayout.status}
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        </TableContainer>
      </Box>
    </Flex>
  );
}
