const referrals = [
  {
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    program: 'Standard',
    comission: '30',
    since: '2021 Dec 24th',
    status: 'active',
  },
  {
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    program: 'Standard',
    comission: '30',
    since: '2021 Dec 24th',
    status: 'active',
  },
  {
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    program: 'Standard',
    comission: '30',
    since: '2021 Dec 24th',
    status: 'active',
  },
  {
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    program: 'Standard',
    comission: '30',
    since: '2021 Dec 24th',
    status: 'active',
  },
  {
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    program: 'Standard',
    comission: '30',
    since: '2021 Dec 24th',
    status: 'active',
  },
  {
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    program: 'Standard',
    comission: '30',
    since: '2021 Dec 24th',
    status: 'active',
  },
];

const pendingPayouts = [
  {
    paidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    paidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    paidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    paidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    paidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
];

const paidPayouts = [
  {
    smartifyAppsPaidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    smartifyAppsPaidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    smartifyAppsPaidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    smartifyAppsPaidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
  {
    smartifyAppsPaidAt: '2020 Dec 23rd',
    shop: 'nate-training-store',
    shopPlan: 'Expert',
    installedApp: 'LAI Product Reviews',
    comission: '30',
    comissionValue: '30',
    status: 'active',
  },
];

export { referrals, pendingPayouts, paidPayouts };
