import React from 'react';
import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import AppFrame from './layouts/AppFrame';

const theme = extendTheme({
  sizes: {
    dashboardContentWidth: '1024px',
  },
  colors: {
    darkBlue: '#2B2D42',
    darkBlueHover: '#3F4261',
    yellow: '#FFB400',
    textColor: 'rgb(25, 32, 43)',
    secondaryTextColor: 'rgba(25, 32, 43, 0.702)',
    primaryColor: '#0e8cff',
    cardBorder: '#e9e9ef',
  },
  components: {
    Input: {
      defaultProps: {
        focusBorderColor: '#2B2D42',
      },
    },
    Textarea: {
      defaultProps: {
        focusBorderColor: '#2B2D42',
      },
    },
    Button: {
      baseStyle: {
        lineHeight: '0.5',
      },
    },
  },
});

function App() {
  return (
    <ChakraProvider theme={theme}>
      <AppFrame />
    </ChakraProvider>
  );
}

export default App;
