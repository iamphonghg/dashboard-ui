import React from 'react';
import { Box, Flex } from '@chakra-ui/react';
import { Outlet } from 'react-router-dom';
import SideBar from '../components/SideBar';
import TopBar from '../components/TopBar';

export default function PartnerDashboardLayout() {
  return (
    <Flex
      flexWrap="nowrap"
      maxHeight="100vh"
      overflowX="auto"
      overflowY="hidden"
      bg="gray.100"
    >
      <SideBar />
      <Box width="100%" overflowY="scroll">
        <TopBar />
        <Outlet />
      </Box>
    </Flex>
  );
}
