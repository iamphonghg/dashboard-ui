import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Apps from '../pages/Apps';
import Dashboard from '../pages/Dashboard';
import Payouts from '../pages/Payouts';
import Referrals from '../pages/Referrals';
import PartnerDashboardLayout from './PartnerDashboardLayout';

export default function AppFrame() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/dashboard" element={<PartnerDashboardLayout />}>
          <Route element={<Dashboard />} index />
          <Route path="apps" element={<Apps />} />
          <Route path="referrals" element={<Referrals />} />
          <Route path="payouts" element={<Payouts />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
